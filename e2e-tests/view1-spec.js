"use strict";
var e2etest = require('./view1-object.js');
describe('my-app', function () {
    var page;

    beforeEach(function () {
        page = new e2etest();
    });

    it('testing add new user', function () {
        var amountBefore = page.countUsers();

        page.typeName('Ablabla');
        page.typeSurname('Zak');
        page.typePesel('92112705698');
        page.selectGender().click();
        page.addButton().click();

        expect(amountBefore).toBeLessThan(page.countUsers());
    });

    it('testing cancel button ', function () {
        var canButton = page.cancelUsers();

        page.typeName('Ablabla');
        page.typeSurname('Zak');
        page.typePesel('92112705698');
        page.selectGender().click();
        page.cancelButton().click();

        expect(canButton).toBe(page.cancelUsers());
    });

    it('testing edit user', function () {
        var amountBefore = page.countUsers();

        page.editButton().click();
        page.typeSurname('STDDD');
        page.selectGender().click();
        page.addButton().click();

        expect(page.typeSurname).toBe(page.typeSurname);
    });

    it('testing remove user', function () {
        var amountBefore = page.countUsers();

        page.selectUserRemove();
        page.removeButtonClick();

        expect(amountBefore).toBeGreaterThan(page.countUsers());
    });
});