'use strict';

var e2etest = function () {
    browser.get('index.html#!/view1');
};

e2etest.prototype = Object.create({}, {
    yourName: {
        get: function () {
            return element(by.model('newItem.name'));
        }
    },
    yourSurname: {
        get: function () {
            return element(by.model('newItem.surname'));
        }
    },
    yourPesel: {
        get: function () {
            return element(by.model('newItem.pesel'));
        }
    },
    addButton: {
        value: function () {
            return element(by.css('#buttonSend')).click();
        }
    },
    editButton: {
        value: function () {
            return element(by.css('#buttonEdit')).click();
        }
    },
    cancelButton:{
        value:function () {
            return element(by.css('#buttonCancel')).click();
        }
    },
    removeButtonClick:{
        value: function () {
            return element(by.css('#buttonRemove')).click();
        }
    },
    selectGender: {
        value: function () {
            return element(by.css('.gender')).click();
        }
    },
    selectUserRemove: {
        value: function () {
            return element(by.css('.checkRemove')).click();
        }
    },
    typeName: {
        value: function (keys) {
            return this.yourName.sendKeys(keys);
        }
    },
    typeSurname: {
        value: function (keys) {
            return this.yourSurname.sendKeys(keys);
        }
    },
    typePesel: {
        value: function (keys) {
            return this.yourPesel.sendKeys(keys);
        }
    },
    countUsers: {
        value: function () {
            return element.all(by.css('.table tr')).count();
        }
    },
   cancelUsers:{
        value: function(){
           return element.all(by.css('.lbl')).count();
        }
    },

});
module.exports = e2etest;